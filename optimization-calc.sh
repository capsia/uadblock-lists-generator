#!/bin/bash
echo -n "Merged lines count: "
cat $@ | sort | uniq | wc -l
echo -n "Duplicated lines count: "
cat $@ | sort | uniq -d | wc -l
echo -n "Not optimized merge lines count: "
cat $@ | wc -l

for file in "$@"
do
    echo -n "Lines in $file: ";
    cat $file | wc -l;
done
