#!/bin/bash
echo "Filtering rules using filter.sed ..."
sed -i -f filter.sed ./lists/*
echo "Sorting files ..."
find ./lists -maxdepth 1 -type f -exec sort -o {} {} \;
echo "Rename files to numbers only..."
for file in ./lists/*
do
  baseFileName="$(basename $file)"
  basePath="$(dirname $file)"
  mv "$basePath/$baseFileName" "$basePath/${baseFileName:0:2}"
done
