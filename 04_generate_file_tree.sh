#!/bin/bash

SECONDS=0

listOfFiles=(./lists/*)
currentCombination=(-1)
stringifiedNumber="00"
lineCount=1

make_combinations()
{
  if (( "${currentCombination[-1]}" == "${#listOfFiles[@]}"))
  then return
  else
    local currentIndex=$((${currentCombination[-1]} + 1))

    if (( "${currentCombination[-1]}" == "-1"))
    then unset currentCombination[-1]
    fi

    echo ${currentCombination[@]}

    while (( "$currentIndex" != "${#listOfFiles[@]}"))
    do
      currentCombination+=( $currentIndex )
      get_filenames

      if (( "$lineCount" == "0" ))
      then
        local currentIndex=$(( currentIndex + 1 ))
        unset currentCombination[-1]
        continue
      fi

      make_combinations

      local currentIndex=$(( currentIndex + 1 ))
      unset currentCombination[-1]
    done
  fi
}

to_zero_prefixed_string()
{
  stringifiedNumber=$( printf '%02d' $(( $1 + 1 )) )
}

get_filenames()
{
  if (( "${#currentCombination[@]}" == "1" ))
  then 
    lineCount=1
    return
  fi

  local currentIndex=0
  local first_file=""

  while (( "$currentIndex" != "$(( ${#currentCombination[@]} - 1))" ))
  do
    to_zero_prefixed_string ${currentCombination[$currentIndex]}
    first_file+="_"$stringifiedNumber
    local currentIndex=$(( currentIndex + 1 ))
  done
  first_file="${first_file:1}"

  ./03_merge_files.sh "./lists/$first_file" ${listOfFiles[${currentCombination[-1]}]} "lists"

  to_zero_prefixed_string ${currentCombination[-1]}
  if [[ -f "./lists/${first_file}_${stringifiedNumber}" ]]; then
    lineCount=1
  else
    lineCount=0
  fi
}

make_combinations

find ./lists/ -type f -empty -delete

duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

ls ./lists > ./lists/index
