/^[[:blank:]]*#/d
s/#.*//
s/\s*$//
s/  \+/ /g
s/\t/ /g
s/^127\.0\.0\.1/0\.0\.0\.0/
/^0\.0\.0\.0/!d
/ local$/d
/ localhost$/d
/ localhost\.localdomain$/d
/ ff$/d
/ fe$/d
/ 0\.0\.0\.0$/d
