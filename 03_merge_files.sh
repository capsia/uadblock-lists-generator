#!/bin/bash

if [[ ! -f $1 ]] || [[ ! -f $2 ]]; then
  exit
fi

leftFile="$(basename $1)"
rightFile="$(basename $2)"
targetFile="$3/${leftFile}_${rightFile}"

commonData=$(comm -12 $1 $2)

if [[ -n "$commonData" ]]; then
  echo "$commonData" > $targetFile

  leftData=$(comm -23 $1 "$3/${leftFile}_${rightFile}")
  rightData=$(comm -23 $2 "$3/${leftFile}_${rightFile}")

  if [[ -n "$leftData" ]]; then
    echo "$leftData" > "$3/$leftFile"
  else
    rm "$3/$leftFile"
  fi

  if [[ -n "$rightData" ]]; then
    echo "$rightData" > "$3/$rightFile"
  else
    rm "$3/$rightFile"
  fi
fi
